﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;

namespace topalovici.Klase
{
    public class Parcela
    {
        public ObjectId _id { get; set; }
        public MongoDBRef groblje { get; set; }
        public List<MongoDBRef> preminuli { get; set; }
        public string oznaka { get; set; }

        public Parcela()
        {
            preminuli = new List<MongoDBRef>();
        }

    }
}