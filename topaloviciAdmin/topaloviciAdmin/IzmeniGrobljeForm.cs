﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Driver.Builders;

namespace topaloviciAdmin
{
    public partial class IzmeniGrobljeForm : Form
    {
        String naziv;
        String adresa;
        Form1 parent;
        public IzmeniGrobljeForm(Form1 f, String n)
        {
            InitializeComponent();
            parent = f;
            naziv = n;
            label3.Text = "Izmeni groblje";
            ucitajGroblje();
        }
        private void izmeniGroblje()
        {
            if(textBoxNaziv.Text=="" || textBoxAdresa.Text == "")
            {
                MessageBox.Show("Morate da unesete sve vrednosti");
            }
            else { 
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var coll = db.GetCollection<Groblje>("Groblje");

            if (naziv == textBoxNaziv.Text && adresa!=textBoxAdresa.Text)
            {
                MessageBox.Show("Promenili ste adresu");
                var query = Query.EQ("naziv", BsonValue.Create(textBoxNaziv.Text)); 
                var update = MongoDB.Driver.Builders.Update.Set("adresa", BsonValue.Create(textBoxAdresa.Text));
                coll.Update(query, update);
            }
            else if(adresa==textBoxAdresa.Text && naziv!=textBoxNaziv.Text)
            {
                MessageBox.Show("Promenili ste naziv");
                var query = Query.EQ("adresa", BsonValue.Create(textBoxAdresa.Text));
                var update = MongoDB.Driver.Builders.Update.Set("naziv", BsonValue.Create(textBoxNaziv.Text));
                coll.Update(query, update);
            }
            else if(adresa==textBoxAdresa.Text && naziv==textBoxNaziv.Text) 
            {
                MessageBox.Show("Niste nista promenili!");
            }
            else
            {
                MessageBox.Show("Promenili ste oba");
                var query = Query.EQ("adresa", BsonValue.Create(adresa));
                var update1 = MongoDB.Driver.Builders.Update.Set("naziv", BsonValue.Create(textBoxNaziv.Text));
                coll.Update(query, update1);
                var update2 = MongoDB.Driver.Builders.Update.Set("adresa", BsonValue.Create(textBoxAdresa.Text));
                coll.Update(query, update2);
            }

            parent.selektovanoGroblje = textBoxNaziv.Text;
            parent.popuniListBoxGroblje();
            this.Close();
            }
        }

        private void ucitajGroblje()
        {
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var coll = db.GetCollection<Groblje>("Groblje");
            var result = coll.Find(Query.EQ("naziv", BsonValue.Create(naziv)));
            if (result.Size() != 0)
            {
                Groblje g = result.ToList<Groblje>()[0];
                textBoxAdresa.Text = g.adresa;
                adresa = g.adresa;
                textBoxNaziv.Text = g.naziv;
            }
        }

        private void buttonIzmeniGroblje_Click(object sender, EventArgs e)
        {
            izmeniGroblje();
        }
    }
}
