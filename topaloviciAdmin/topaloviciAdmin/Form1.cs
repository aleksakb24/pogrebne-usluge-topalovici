﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace topaloviciAdmin
{
    public partial class Form1 : Form
    {
        private List<Preminuli> preminuleOsobeNaSelektovanojParceli;
        public String selektovanoGroblje="";
        public String selektovanaParcela="";
        public Form1()
        {
            InitializeComponent();
            preminuleOsobeNaSelektovanojParceli = new List<Preminuli>();
            popuniListBoxGroblje();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DodajGrobljeForm addGroblje = new DodajGrobljeForm(this);
            addGroblje.Show();
        }

        public void popuniListBoxGroblje()
        {
            listBoxParcele.Items.Clear();
            listBoxPreminuli.Items.Clear();
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collection = db.GetCollection<Groblje>("Groblje");
            var collectionP = db.GetCollection<Parcela>("Parcela");
            List<Groblje> groblja = collection.FindAll().ToList < Groblje>();
            listBoxGroblje.Items.Clear();
            foreach(Groblje gr in groblja)
            {
                listBoxGroblje.Items.Add(gr.naziv);
            }
            Groblje g = collection.FindOne(Query.EQ("naziv",selektovanoGroblje));
            Parcela p = collectionP.FindOne(Query.EQ("oznaka", selektovanaParcela));
            if(g==null)
            {
                listBoxParcele.Items.Clear();
                listBoxPreminuli.Items.Clear();
            }
            else
            {
                listBoxGroblje.SelectedItem = selektovanoGroblje;
                popuniParcele();
                if (p != null) {
                    listBoxParcele.SelectedItem = selektovanaParcela;
                    popuniPreminule();
                }
            }
        }

        private void buttonBrisiGroblje_Click(object sender, EventArgs e)
        {
            if(listBoxGroblje.SelectedIndex != -1)
            {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");

                var coll = db.GetCollection<Groblje>("Groblje");
                var collparc = db.GetCollection<Parcela>("Parcela");
                var collpre = db.GetCollection<Preminuli>("Preminuli");
                Groblje g = coll.FindOne(Query.EQ("naziv", BsonValue.Create(listBoxGroblje.SelectedItem.ToString())));
            
                foreach(MongoDBRef parc in g.parcele)
                {
                    Parcela p = collparc.FindOne(Query.EQ("_id", BsonValue.Create(parc.Id)));
                    foreach (MongoDBRef prem in p.preminuli)
                    {
                        collpre.Remove(Query.EQ("_id", BsonValue.Create(prem.Id)));
                    }
                    collparc.Remove(Query.EQ("_id",BsonValue.Create(p._id)));
                }
                 coll.Remove(Query.EQ("_id", BsonValue.Create(g._id)));
                popuniListBoxGroblje();
            }
            else
            {
                MessageBox.Show("Izaberite groblje");
            }
        }

        private void buttonUpdateGroblje_Click(object sender, EventArgs e)
        {
            if (listBoxGroblje.SelectedIndex != -1)
            {
                IzmeniGrobljeForm gf = new IzmeniGrobljeForm(this, listBoxGroblje.SelectedItem.ToString());
                
                gf.Show();
            }
            else
            {
                MessageBox.Show("Izaberite groblje");
            }
        }

        private void buttonDodajParcelu_Click(object sender, EventArgs e)
        {
 
            if (listBoxGroblje.SelectedIndex == -1) 

            { 
                MessageBox.Show("Morate prvo da izaberete groblje"); 
            }
            else { 
                String nazivGroblja = listBoxGroblje.SelectedItem.ToString();
                DodajParceluForm dp = new DodajParceluForm(nazivGroblja, this);
                dp.Show();
            }
        }

        private void listBoxGroblje_MouseClick(object sender, MouseEventArgs e)
        {
            if (listBoxGroblje.SelectedIndex != -1)
            {
                selektovanoGroblje = listBoxGroblje.SelectedItem.ToString();
                listBoxPreminuli.Items.Clear();
                popuniParcele();
             
            }
            else
            {
                MessageBox.Show("Izaberite groblje");
                listBoxParcele.Items.Clear();
                listBoxPreminuli.Items.Clear();
            }
        }

        private void buttonDodajPreminulog_Click(object sender, EventArgs e)
        {
            if(listBoxGroblje.SelectedItems.Count==0 || listBoxParcele.SelectedItems.Count == 0)
            {
                MessageBox.Show("Morate selektovati i groblje i parcelu");
            }
            else
            {
                DodajPreminulogForm dpf = new DodajPreminulogForm(this, listBoxGroblje.SelectedItem.ToString(), listBoxParcele.SelectedItem.ToString());
                dpf.Show();
            }
        }

        private void listBoxParcele_MouseClick(object sender, MouseEventArgs e)
        {
            if (listBoxParcele.SelectedIndex != -1)
            {
                selektovanaParcela = listBoxParcele.SelectedItem.ToString();
                popuniPreminule();
            }
            else
            {
                MessageBox.Show("Izaberite parcelu");
            }
        }

        private void buttonAzurirajParcelu_Click(object sender, EventArgs e)
        {
            if (listBoxParcele.SelectedItems.Count != 0 && listBoxGroblje.SelectedItems.Count!=0) 
            { 
                IzmeniParceluForm ip = new IzmeniParceluForm(this, listBoxParcele.SelectedItem.ToString());
                ip.Show();
            }
            else
            {
                MessageBox.Show("Morate izabrati groblje i parcelu");
            }
        }

        private void popuniParcele()
        {
            listBoxParcele.Items.Clear();
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collectionGroblje = db.GetCollection<Groblje>("Groblje");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            var query = Query.EQ("naziv", selektovanoGroblje);
            Groblje groblje = collectionGroblje.FindOne(query);

            var queryParcela = from parcela in collectionParcela.AsQueryable<Parcela>()
                               where parcela.groblje.Id == groblje._id
                               select parcela.oznaka;

            foreach (String p in queryParcela.ToList<String>())
                listBoxParcele.Items.Add(p);
        }
        private void popuniPreminule()
        {
            listBoxPreminuli.Items.Clear();
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            var collectionPreminuli = db.GetCollection<Preminuli>("Preminuli");
            var query = Query.EQ("oznaka", selektovanaParcela);
            Parcela parcela = collectionParcela.FindOne(query);
            if (parcela == null) { listBoxPreminuli.Items.Clear(); }
            else
            {
                var queryPreminuli = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                     where preminuli.parcela.Id == parcela._id
                                     select preminuli;
                preminuleOsobeNaSelektovanojParceli = queryPreminuli.ToList<Preminuli>();
                foreach (Preminuli p in preminuleOsobeNaSelektovanojParceli)
                {
                    String item = p.ime;
                    listBoxPreminuli.Items.Add(item);
                }
            }
        }

        private void buttonAzurirajPreminulog_Click(object sender, EventArgs e)
        {
            if (listBoxPreminuli.SelectedIndex == -1)
            {
                MessageBox.Show("Selektujte preminulu osobu");
            }
            else { 
            Preminuli p = preminuleOsobeNaSelektovanojParceli[listBoxPreminuli.SelectedIndex];
            IzmeniPreminulogForm ip = new IzmeniPreminulogForm(this, p);
            ip.Show();
            }
        }

        private void buttonBrisiPreminulog_Click(object sender, EventArgs e)
        {
            if(listBoxPreminuli.SelectedIndex == -1)
            {
                MessageBox.Show("Izaberite preminulog");
            }
            else
            {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");

                var coll = db.GetCollection<Preminuli>("Preminuli");
                var collp = db.GetCollection<Parcela>("Parcela");

                Preminuli zaBrisanje = preminuleOsobeNaSelektovanojParceli[listBoxPreminuli.SelectedIndex];
                Parcela p= collp.FindOne(Query.EQ("_id", BsonValue.Create(zaBrisanje.parcela.Id)));
                foreach(MongoDBRef pr in p.preminuli)
                {
                    if (pr.Id == zaBrisanje._id)
                    {
                        p.preminuli.Remove(pr);
                        collp.Save(p);
                        break;
                    }
                }
                coll.Remove(Query.EQ("_id", BsonValue.Create(zaBrisanje._id)));
                popuniListBoxGroblje();
            }
        }

        private void buttonBrisiParcelu_Click(object sender, EventArgs e)
        {
            if (listBoxParcele.SelectedIndex == -1)
            {
                MessageBox.Show("Izaberite parcelu");
            }
            else
            {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");

                var coll = db.GetCollection<Preminuli>("Preminuli");
                var collp = db.GetCollection<Parcela>("Parcela");
                var collg = db.GetCollection<Groblje>("Groblje");
                Parcela p = collp.FindOne(Query.EQ("oznaka", BsonValue.Create(listBoxParcele.SelectedItem)));

                foreach (MongoDBRef pr in p.preminuli)
                {
                    coll.Remove(Query.EQ("_id", BsonValue.Create(pr.Id)));

                }

                Groblje g= collg.FindOne(Query.EQ("_id",BsonValue.Create(p.groblje.Id)));
                foreach (MongoDBRef parc in g.parcele)
                {
                    if (parc.Id == p._id)
                    {
                        g.parcele.Remove(parc);
                        collg.Save(g);
                        break;
                    }
                }
                
                collp.Remove(Query.EQ("_id", BsonValue.Create(p._id)));

                popuniListBoxGroblje();
            }
        }

        private void buttonSpisakUsluga_Click(object sender, EventArgs e)
        {
            SpisakUslugaForm f = new SpisakUslugaForm();
            f.Show();
        }

        private void buttonSpisakPorudzbina_Click(object sender, EventArgs e)
        {
            SpisakPorudzbinaForm f = new SpisakPorudzbinaForm();
            f.Show();
        }
    }
}
