﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace topaloviciAdmin.Collections
{
    public class Usluga
    {
        public ObjectId _id { get; set; }
        public string naziv { get; set; }
        public int cena { get; set; }
    }
}