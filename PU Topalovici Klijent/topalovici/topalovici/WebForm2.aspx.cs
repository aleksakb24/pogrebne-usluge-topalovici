﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using topalovici.Klase;


namespace topalovici
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                FillTable();
                fillDDLGroblje();
            }
        }

        protected void FillTable()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var db = server.GetDatabase("Topalovici");

            var collectionPreminuli = db.GetCollection<Preminuli>("Preminuli");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            var collectionGroblje = db.GetCollection<Groblje>("Groblje");

            DataTable dt = new DataTable();
            dt.Columns.Add("Ime i Prezime", typeof(string));
            dt.Columns.Add("Pol", typeof(string));
            dt.Columns.Add("Datum Rodjenja", typeof(string));
            dt.Columns.Add("Datum Smrti", typeof(string));
            dt.Columns.Add("Groblje", typeof(string));
            dt.Columns.Add("Parcela", typeof(string));

            foreach (Preminuli p in collectionPreminuli.FindAll())
            {
                Parcela par = collectionParcela.FindOneById(p.parcela.Id);
                Groblje gro = collectionGroblje.FindOneById(par.groblje.Id);
                dt.Rows.Add(p.ime, p.pol, p.datRodj, p.datSmrti,gro.naziv + ", " + gro.adresa,par.oznaka);
            }

            tabPreminuli.DataSource = dt;
            tabPreminuli.DataBind();
        }

        protected void fillDDLGroblje()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var db = server.GetDatabase("Topalovici");

            var collectionGroblje = db.GetCollection<Groblje>("Groblje");

            ListItemCollection ddlGroblja = new ListItemCollection();
            ddlGroblja.Add(new ListItem("Izaberite Groblje", "Izaberite Groblje"));

            foreach (Groblje g in collectionGroblje.FindAll())            
                ddlGroblja.Add(new ListItem(g.naziv, g.naziv));

            ddlGroblje.DataSource = ddlGroblja;
            ddlGroblje.DataBind();

            
        }

        protected void fillDDLParcele(string grobljeNaziv)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var db = server.GetDatabase("Topalovici");

            var collectionGroblje = db.GetCollection<Groblje>("Groblje");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");

            ListItemCollection ddlParcela = new ListItemCollection();
            ddlParcela.Add(new ListItem("Izaberite Parcelu", "Izaberite Parcelu"));

            var queryGroblje = Query.EQ("naziv", grobljeNaziv);
            Groblje groblje = collectionGroblje.FindOne(queryGroblje);

            var queryParcela = from parcela in collectionParcela.AsQueryable<Parcela>()
                               where parcela.groblje.Id == groblje._id
                               select parcela;

            foreach (Parcela p in queryParcela)
                ddlParcela.Add(new ListItem(p.oznaka, p.oznaka));

            ddlParcele.DataSource = ddlParcela;
            ddlParcele.DataBind();

        }


        protected void btnPretrazi_Click(object sender, EventArgs e)
        {

            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var db = server.GetDatabase("Topalovici");

            var collectionPreminuli = db.GetCollection<Preminuli>("Preminuli");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            var collectionGroblje = db.GetCollection<Groblje>("Groblje");

            DataTable dt = new DataTable();
            dt.Columns.Add("Ime i Prezime", typeof(string));
            dt.Columns.Add("Pol", typeof(string));
            dt.Columns.Add("Datum Rodjenja", typeof(string));
            dt.Columns.Add("Datum Smrti", typeof(string));
            dt.Columns.Add("Groblje", typeof(string));
            dt.Columns.Add("Parcela", typeof(string));

            IQueryable<Preminuli> query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                          select preminuli;

            if (txtSearch.Text == "")
            {

                if (ddlGroblje.SelectedValue == "Izaberite Groblje")
                {

                    if (ddlDan.SelectedValue != "Dan" && ddlMesec.SelectedValue != "Mesec" && ddlGodina.SelectedValue != "Godina")
                    {
                        query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                where preminuli.datSmrti.Equals(ddlDan.SelectedValue + "." + ddlMesec.SelectedValue + "." + ddlGodina.SelectedValue + ".")
                                select preminuli;
                    }
                }
                else
                {
                    var queryG = Query.EQ("naziv", ddlGroblje.SelectedValue);
                    Groblje g = collectionGroblje.FindOne(queryG);

                    if (ddlParcele.SelectedValue == "Izaberite Parcelu")
                    {
                        

                        if (ddlDan.SelectedValue != "Dan" && ddlMesec.SelectedValue != "Mesec" && ddlGodina.SelectedValue != "Godina")
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.datSmrti.Equals(ddlDan.SelectedValue + "." + ddlMesec.SelectedValue + "." + ddlGodina.SelectedValue + ".")
                                    && preminuli.groblje.Id == g._id
                                    select preminuli;
                        }
                        else
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.groblje.Id == g._id
                                    select preminuli;
                        }
                    }
                    else
                    {
                        var queryP = from parcela in collectionParcela.AsQueryable<Parcela>()
                                     where parcela.oznaka == ddlParcele.SelectedValue
                                     && parcela.groblje.Id == g._id
                                     select parcela;
                        Parcela p = queryP.First();

                        if (ddlDan.SelectedValue != "Dan" && ddlMesec.SelectedValue != "Mesec" && ddlGodina.SelectedValue != "Godina")
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.datSmrti.Equals(ddlDan.SelectedValue + "." + ddlMesec.SelectedValue + "." + ddlGodina.SelectedValue + ".")
                                    && preminuli.groblje.Id == g._id
                                    && preminuli.parcela.Id == p._id
                                    select preminuli;
                        }
                        else
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.groblje.Id == g._id
                                    && preminuli.parcela.Id == p._id
                                    select preminuli;
                        }
                    }
                }
            }
            else
            {
                if (ddlGroblje.SelectedValue == "Izaberite Groblje")
                {

                    if (ddlDan.SelectedValue != "Dan" && ddlMesec.SelectedValue != "Mesec" && ddlGodina.SelectedValue != "Godina")
                    {
                        query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                where preminuli.datSmrti.Equals(ddlDan.SelectedValue + "." + ddlMesec.SelectedValue + "." + ddlGodina.SelectedValue + ".")
                                && preminuli.ime.Contains(txtSearch.Text)
                                select preminuli;
                      
                    }
                    else
                    {
                        query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                where preminuli.ime.Contains(txtSearch.Text)
                                select preminuli;
                    }
                }
                else
                {
                    var queryG2 = Query.EQ("naziv", ddlGroblje.SelectedValue);
                    Groblje g2 = collectionGroblje.FindOne(queryG2);

                    if (ddlParcele.SelectedValue == "Izaberite Parcelu")
                    {


                        if (ddlDan.SelectedValue != "Dan" && ddlMesec.SelectedValue != "Mesec" && ddlGodina.SelectedValue != "Godina")
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.datSmrti.Equals(ddlDan.SelectedValue + "." + ddlMesec.SelectedValue + "." + ddlGodina.SelectedValue + ".")
                                    && preminuli.groblje.Id == g2._id
                                    && preminuli.ime.Contains(txtSearch.Text)
                                    select preminuli;
                        }
                        else
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.groblje.Id == g2._id
                                     && preminuli.ime.Contains(txtSearch.Text)
                                    select preminuli;
                        }
                    }
                    else
                    {
                        var queryP2 = from parcela in collectionParcela.AsQueryable<Parcela>()
                                     where parcela.oznaka == ddlParcele.SelectedValue
                                     && parcela.groblje.Id == g2._id
                                     select parcela;
                        Parcela p2 = queryP2.First();

                        if (ddlDan.SelectedValue != "Dan" && ddlMesec.SelectedValue != "Mesec" && ddlGodina.SelectedValue != "Godina")
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.datSmrti.Equals(ddlDan.SelectedValue + "." + ddlMesec.SelectedValue + "." + ddlGodina.SelectedValue + ".")
                                    && preminuli.groblje.Id == g2._id
                                    && preminuli.parcela.Id == p2._id
                                    && preminuli.ime.Contains(txtSearch.Text)
                                    select preminuli;
                        }
                        else
                        {
                            query = from preminuli in collectionPreminuli.AsQueryable<Preminuli>()
                                    where preminuli.groblje.Id == g2._id
                                    && preminuli.parcela.Id == p2._id
                                    && preminuli.ime.Contains(txtSearch.Text)
                                    select preminuli;
                        }
                    }
                }
            }

            foreach (Preminuli p in query)
            {
                Parcela par = collectionParcela.FindOneById(p.parcela.Id);
                Groblje gro = collectionGroblje.FindOneById(par.groblje.Id);
                dt.Rows.Add(p.ime, p.pol, p.datRodj, p.datSmrti, gro.naziv + ", " + gro.adresa, par.oznaka);
            }

            tabPreminuli.DataSource = dt;
            tabPreminuli.DataBind();

        }

        protected void btnBack_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("WebForm1.aspx");
        }

        protected void ddlGroblje_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlGroblje.SelectedValue != "Izaberite Groblje")
            {
                fillDDLParcele(ddlGroblje.SelectedValue);
            }
            else
            {
                ListItemCollection ddlParcela = new ListItemCollection();
                ddlParcela.Add(new ListItem("Izaberite Parcelu", "Izaberite Parcelu"));
                ddlParcele.DataSource = ddlParcela;
                ddlParcele.DataBind();
                ddlParcele.SelectedIndex = 0;
            }
        }
    }
}