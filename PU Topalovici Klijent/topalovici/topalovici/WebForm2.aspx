﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="topalovici.WebForm2" %>

<!DOCTYPE html>
<style>
    .drop {
        background-color: #383838;
        border: 1px solid #FFD48A;
        color: #FFD48A;
        font-size: 20px;
        font-family: 'Playfair Display', serif;
        margin-left: 5px;
        margin-right: 5px;
    }

    .search {
        height: auto;
        width: 40%;
        background: transparent;
        border-bottom: 1px solid #FFD48A;
        border-top: 0;
        border-right: 0;
        border-left: 0;
        font-size: 20px;
        font-family: 'Playfair Display', serif;
    }

    ::-webkit-input-placeholder {
        color: #8d8d8d;
    }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet" />
    <link rel="Stylesheet" href="/pretraga.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" style="height: 100%; width: 100%;">
        <div class="main-div">
            <div class="header">
                <asp:ImageButton ID="btnBack" runat="server" ImageUrl="/assets/back.png" Height="50px" Width="50px" OnClick="btnBack_Click"/>
                <div class="logo-compl">
                    <p class="logo">TOPALOVIĆI</p>
                    <p class="logo2">usluge i pretraga</p>
                </div>
                <div class="menu-icon">
                    <div class="line"></div>
                    <div class="line"></div>
                </div>
            </div>
            <div class="content">
                <div class="cont-one">
                    <div class="cont-head">
                        <p class="logo">
                            PRETRAGA
                        </p>
                    </div>
                    <div class="cont-form">
                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Pretrazi po imenu.." CssClass="search"></asp:TextBox>
                        <div class="groblje">
                            <asp:DropDownList ID="ddlGroblje" runat="server" CssClass="drop" AutoPostBack="True" OnSelectedIndexChanged="ddlGroblje_SelectedIndexChanged">

                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlParcele" runat="server" CssClass="drop">
                                <asp:ListItem>Izaberite Parcelu</asp:ListItem>

                            </asp:DropDownList>
                        </div>
                        <div class="groblje">
                            <p class="dat">
                                Datum smrti
                            </p>
                            <asp:DropDownList ID="ddlDan" runat="server" CssClass="drop">
                                <asp:ListItem >Dan</asp:ListItem>
                                <asp:ListItem >01</asp:ListItem>
                                <asp:ListItem >02</asp:ListItem>
                                <asp:ListItem>03</asp:ListItem>
                                <asp:ListItem>04</asp:ListItem>
                                <asp:ListItem>05</asp:ListItem>
                                <asp:ListItem>06</asp:ListItem>
                                <asp:ListItem>07</asp:ListItem>
                                <asp:ListItem>08</asp:ListItem>
                                <asp:ListItem>09</asp:ListItem>
                                <asp:ListItem>10</asp:ListItem>
                                <asp:ListItem>11</asp:ListItem>
                                <asp:ListItem>12</asp:ListItem>
                                <asp:ListItem>13</asp:ListItem>
                                <asp:ListItem>14</asp:ListItem>
                                <asp:ListItem>15</asp:ListItem>
                                <asp:ListItem>16</asp:ListItem>
                                <asp:ListItem>17</asp:ListItem>
                                <asp:ListItem>18</asp:ListItem>
                                <asp:ListItem>19</asp:ListItem>
                                <asp:ListItem>20</asp:ListItem>
                                <asp:ListItem>21</asp:ListItem>
                                <asp:ListItem>22</asp:ListItem>
                                <asp:ListItem>23</asp:ListItem>
                                <asp:ListItem>24</asp:ListItem>
                                <asp:ListItem>25</asp:ListItem>
                                <asp:ListItem>26</asp:ListItem>
                                <asp:ListItem>27</asp:ListItem>
                                <asp:ListItem>28</asp:ListItem>
                                <asp:ListItem>29</asp:ListItem>
                                <asp:ListItem>30</asp:ListItem>
                                <asp:ListItem>31</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlMesec" runat="server" CssClass="drop">
                                <asp:ListItem >Mesec</asp:ListItem>
                                <asp:ListItem Value="01" >Januar</asp:ListItem>
                                <asp:ListItem Value="02" >Februar</asp:ListItem>
                                <asp:ListItem Value="03">Mart</asp:ListItem>
                                <asp:ListItem Value="04">April</asp:ListItem>
                                <asp:ListItem Value="05">Maj</asp:ListItem>
                                <asp:ListItem Value="06">Jun</asp:ListItem>
                                <asp:ListItem Value="07">Jul</asp:ListItem>
                                <asp:ListItem Value="08">Avgust</asp:ListItem>
                                <asp:ListItem Value="09">Septembar</asp:ListItem>
                                <asp:ListItem Value="10">Oktobar</asp:ListItem>
                                <asp:ListItem Value="11">Novembar</asp:ListItem>
                                <asp:ListItem Value="12">Decembar</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlGodina" runat="server" CssClass="drop">
                                <asp:ListItem >Godina</asp:ListItem>
                                <asp:ListItem >2017</asp:ListItem>
                                <asp:ListItem >2018</asp:ListItem>
                                <asp:ListItem>2019</asp:ListItem>
                                <asp:ListItem>2020</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="groblje">
                            <asp:Button ID="btnPretrazi" runat="server" Style="background: transparent; border: 1px solid #FFD48A; color: #FFD48A; height: 50px; width: 150px; font-size: 15px; font-family: 'Playfair Display', serif;" Text="PRETRAŽI" OnClick="btnPretrazi_Click" />
                        </div>
                    </div>
                </div>
                <div class="cont-two">
                    <asp:GridView ID="tabPreminuli" runat="server" Style="background: transparent; border: 1px solid #FFD48A; color: #FFD48A; margin-top:30px;" AutoGenerateColumns="False">

                        <Columns>
                            <asp:BoundField DataField="Ime i Prezime" HeaderText="Ime i Prezime" />
                            <asp:BoundField DataField="Pol" HeaderText="Pol" />
                            <asp:BoundField DataField="Datum Rodjenja" HeaderText="Datum Rodjenja" />
                            <asp:BoundField DataField="Datum Smrti" HeaderText="Datum Smrti" />
                            <asp:BoundField DataField="Groblje" HeaderText="Groblje" />
                            <asp:BoundField DataField="Parcela" HeaderText="Parcela" />
                        </Columns>

                    </asp:GridView>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
