﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="topalovici.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="/home.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet" />
</head>
<body>
    <style>
        .card1 {
            height: 100%;
            width: 35%;
            border: 0.1pt solid #FFD48A;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            transition: 0.6s ease-in-out;
            background: transparent;
            color: #FFD48A;
            font-family: 'Playfair Display', serif;
            font-size: 40px;
        }

            .card1:hover {
                background: #FFD48A;
                color: black;
            }
    </style>
    <form runat="server">
        <div class="main-div">
            <div class="left-div"></div>
            <div class="center-div">
                <div style="align-items: center; width: 100%; display: flex; flex-direction: column; margin-top: 5%;">
                    <p style="color: #FFD48A; font-family: 'Playfair Display', serif; font-size: 40px; margin: 0;">TOPALOVIĆI
                    </p>
                    <p style="color: #FFD48A; font-family: 'Playfair Display', serif; font-size: 20px; margin: -10px 0 0 0;">usluge i pretraga</p>
                </div>
                <div class="us-pr-div">

                    <asp:Button ID="btnUsluge" runat="server" CssClass="card1" OnClick="Usluge_Click" Text="USLUGE">
                        <%--  <p class="uspr1">USLUGE</p>
                    <div class="card-uspr-div">
                        <p class="uspr1-txt">Poručite cveće, kopače, prevoz, narikače, sveće i slične usluge koje nudimo.</p>
                    </div>--%>
                    </asp:Button>

                    <asp:Button ID="btnPretraga" runat="server" class="card1" OnClick="Pretraga_Click" Text="PRETRAGA">
                        <%-- <p class="uspr2">PRETRAGA</p>
                    <div class="card-uspr-div">
                        <p class="uspr2-txt">Pretražite grobno mesto konkretne preminule osobe.</p>
                    </div>--%>
                    </asp:Button>
                </div>
                
            </div>
            <div class="right-div"></div>


        </div>
    </form>
</body>
</html>
