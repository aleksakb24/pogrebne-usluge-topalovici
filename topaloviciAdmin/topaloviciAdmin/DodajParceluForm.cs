﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Driver.Builders;

namespace topaloviciAdmin
{
    public partial class DodajParceluForm : Form
    {
        String nazivGroblja;
        Form1 parent;
        public DodajParceluForm(String g, Form1 p)
        {
            InitializeComponent();
            nazivGroblja = g;
            parent = p;
        }

        private void buttonAddParcela_Click(object sender, EventArgs e)
        {
            if (textBox1.Text=="") { MessageBox.Show("Morate da unesete oznaku"); }
            else { 
            Parcela parcela = new Parcela();
            parcela.oznaka = textBox1.Text;
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collectionGroblje = db.GetCollection<Groblje>("Groblje");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            var query = Query.EQ("naziv", nazivGroblja);
            Groblje groblje = collectionGroblje.FindOne(query);
            parcela.groblje = new MongoDBRef("groblje", groblje._id);
            collectionParcela.Insert(parcela);
            var result = collectionParcela.FindOne(Query.EQ("oznaka", BsonValue.Create(parcela.oznaka)));
            
            groblje.parcele.Add(new MongoDBRef("parcele", result._id));
            collectionGroblje.Save(groblje);
            MessageBox.Show("Uspesno upisana nova parcela!");
            parent.popuniListBoxGroblje();
            this.Close();
            }
        }
    }
}
