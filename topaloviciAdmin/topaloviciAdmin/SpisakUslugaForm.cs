﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver;
using MongoDB.Driver.Linq;


namespace topaloviciAdmin
{
    public partial class SpisakUslugaForm : Form
    {
        List<Usluga> listaUsluga;
        public SpisakUslugaForm()
        {
            InitializeComponent();
            osvezi();

        }

        private void buttonAzurirajCenuUsluge_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Izaberite uslugu");
            }
            else
            {
                CenaUslugeForm f = new CenaUslugeForm(this, listaUsluga[listBox1.SelectedIndex]);
                f.Show();
            }
        }
        public void osvezi()
        {
            listBox1.Items.Clear();
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collection = db.GetCollection<Usluga>("Usluga");
            listaUsluga = collection.FindAll().ToList<Usluga>();
            foreach (Usluga u in listaUsluga)
            {
                listBox1.Items.Add(u.naziv + " " + u.cena);
            }
        }
    }
}
