﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
namespace topaloviciAdmin
{
    public partial class SpisakPorudzbinaForm : Form
    {
        int selektovano;
        List<Porudzbina> spisakPorudzbina;
        public SpisakPorudzbinaForm()
        {
            InitializeComponent();
            spisakPorudzbina = new List<Porudzbina>();
            
        }

        private void SpisakPorudzbinaForm_Load(object sender, EventArgs e)
        {
            osvezi();
        }

        

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                if (dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    dataGridView1.CurrentRow.Selected = true;
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Morate da izaberete porudzbinu");
            }
            else
            {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");
                var collection = db.GetCollection<Porudzbina>("Porudzbina");

                foreach (DataGridViewRow d in dataGridView1.SelectedRows)
                {
                    Porudzbina p = spisakPorudzbina[d.Index];
                    collection.Remove(Query.EQ("_id",BsonValue.Create(p._id)));
                }
                osvezi();
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Morate da izaberete porudzbinu");
            }
            else
            {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");
                var collection = db.GetCollection<Porudzbina>("Porudzbina");

                foreach (DataGridViewRow d in dataGridView1.SelectedRows)
                {
                    Porudzbina p = spisakPorudzbina[d.Index];
                    string poruka = "Usluge:\n";
                    foreach (string usluga in p.usluge)
                        poruka += "\n" + usluga;
                     MessageBox.Show(poruka);
                }
            }
        }
        private void osvezi()
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
            dataGridView1.Columns.Add("ime", "Ime");
            dataGridView1.Columns.Add("adresa", "Adresa");
            dataGridView1.Columns.Add("telefon", "Telefon");
            dataGridView1.Columns.Add("racun", "Racun");
            int width = 50;
            foreach (DataGridViewColumn c in dataGridView1.Columns)
            {
                c.Width = 150;
                width += 150;
            }
            this.Width = 750;
            dataGridView1.Width = 650;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");
            var collection = db.GetCollection<Porudzbina>("Porudzbina");
            spisakPorudzbina = collection.FindAll().ToList<Porudzbina>();
            foreach (Porudzbina p in spisakPorudzbina)
            {
                String[] row = { p.ime, p.adresa, p.telefon, p.racun.ToString() };
                dataGridView1.Rows.Add(row);
            }
        }

        
    }
}
