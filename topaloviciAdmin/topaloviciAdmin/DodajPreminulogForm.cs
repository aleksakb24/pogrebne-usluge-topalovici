﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace topaloviciAdmin
{
    public partial class DodajPreminulogForm : Form
    {
        Form1 parent;
        String nazivGroblja;
        String oznakaParcele;
        public DodajPreminulogForm(Form1 p, String g, String parc)
        {
            InitializeComponent();
            parent = p;
            nazivGroblja = g;
            oznakaParcele = parc;
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            if (textBox1.Text=="" ||  textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
            {
                MessageBox.Show("Morate uneti sva polja");
            }
            else { 
            Preminuli p = new Preminuli();
            p.ime = textBox1.Text;
            p.pol = textBox3.Text;
            p.datRodj = textBox4.Text;
            p.datSmrti = textBox5.Text;

            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collectionGroblje = db.GetCollection<Groblje>("Groblje");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            var collectionPreminuli = db.GetCollection<Preminuli>("Preminuli");
            var query = Query.EQ("naziv", nazivGroblja);
            Groblje groblje = collectionGroblje.FindOne(query);
            var query2 = Query.EQ("oznaka", oznakaParcele);
            Parcela parcela = collectionParcela.FindOne(query2);
            p.parcela = new MongoDBRef("parcela", parcela._id);
            p.groblje = new MongoDBRef("groblje", groblje._id);
            collectionPreminuli.Insert(p);

            var query3 = Query.EQ("ime",p.ime);
            Preminuli preminulaOsoba = collectionPreminuli.FindOne(query3);
            parcela.preminuli.Add(new MongoDBRef("preminuli", preminulaOsoba._id));
            collectionParcela.Save(parcela);
            MessageBox.Show("Dodata preminula osoba");
            parent.popuniListBoxGroblje();
            this.Close();
            }
        }

    }
}
