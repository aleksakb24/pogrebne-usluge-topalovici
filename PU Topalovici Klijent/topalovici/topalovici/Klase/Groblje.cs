﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;

namespace topalovici.Klase
{
    public class Groblje
    {
        public ObjectId _id { get; set; }
        public string naziv { get; set; }
        public string adresa { get; set; }
        public List<MongoDBRef> parcele { get; set; }

        public Groblje()
        {
            parcele = new List<MongoDBRef>();
        }
    }
}