﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace topaloviciAdmin.Collections
{
    public class Preminuli
    {
        public ObjectId _id { get; set; }
        public MongoDBRef parcela { get; set; }
        public MongoDBRef groblje { get; set; }
        public string ime { get; set; }
        public string pol { get; set; }
        public string datRodj { get; set; }
        public string datSmrti { get; set; }
    }
}
