﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace topaloviciAdmin.Collections
{
    public class Parcela
    {
        public ObjectId _id { get; set; }
        public MongoDBRef groblje { get; set; }
        public List<MongoDBRef> preminuli { get; set; }
        public string oznaka { get; set; }

        public Parcela()
        {
            preminuli = new List<MongoDBRef>();
        }

    }
}
