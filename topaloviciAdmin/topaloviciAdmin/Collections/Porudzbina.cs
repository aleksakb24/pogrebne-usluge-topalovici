﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace topaloviciAdmin.Collections
{
    public class Porudzbina
    {
        public ObjectId _id { get; set; }
        public string ime { get; set; }
        public string telefon { get; set; }
        public string adresa { get; set; }
        public int racun { get; set; }
        public List<string> usluge { get; set; }

    }
}