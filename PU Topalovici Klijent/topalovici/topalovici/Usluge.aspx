﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Usluge.aspx.cs" Inherits="topalovici.Usluge" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet" />
    <link rel="Stylesheet" href="/usluge.css" type="text/css" />
</head>
<body>
    <style>
        .content {
            width: 90%;
            height: calc(100% - 100px);
            display: flex;
            flex-direction: column;
        }

        .txtBox {
            color: #FFD48A;
            font-size: 20px;
            border: 1px solid #FFD48A;
            background: transparent;
            font-family: 'Playfair Display', serif;
        }

        ::-webkit-input-placeholder {
            color: #8d8d8d;
        }

        .dugme-poruci {
            height: 50px;
            width: 150px;
            border: 1px solid #FFD48A;
            background: transparent;
            color: #FFD48A;
            font-size: 20px;
            font-family: 'Playfair Display', serif;
            transition: 0.4s ease-in-out;
        }

            .dugme-poruci:hover {
                background: #FFD48A;
                color: black;
            }

        .cont-one {
            height: 50%;
            width: 100%;
            margin-bottom: 2%;
        }

        .cont-head {
            margin-top: 1.5%;
            height: 10%;
        }

        .cont-items {
            display: flex;
            height: 75%;
            width: 100%;
            justify-content: space-around;
            flex-wrap: wrap;
            margin-top: 1.5%;
            overflow: auto;
        }

        .item-compl {
            display: flex;
            flex-direction: column;
            align-items: center;
            color: #ffd48a;
            margin: 10px;
        }
    </style>
    <form id="form1" runat="server" style="height: 100vh; width: 100vw;">
    <div class="main-div">
        <div class="header">
            <asp:ImageButton ID="btnBack" runat="server" ImageUrl="/assets/back.png" Height="50px" Width="50px" OnClick="btnBack_Click" />

            <div class="logo-compl">
                <p class="logo">TOPALOVIĆI</p>
                <p class="logo2">usluge i pretraga</p>
            </div>
            <div class="menu-icon">
                <div class="line"></div>
                <div class="line"></div>
            </div>
        </div>
    
        <div class="content">
            <div class="cont-one">
                <div class="cont-head">
                    <p class="logo">
                        USLUGE
                    </p>
                </div>
                <div class="cont-items">
                    <div class="item-compl">
                        <div class="cont-item-1">
                            <p class="item">CVEĆE</p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkCvece" runat="server" AutoPostBack="True" OnCheckedChanged="checkCvece_CheckedChanged" /></div>
                    </div>

                    <div class="item-compl">
                        <div class="cont-item-2">
                            <p class="item">SVEĆE</p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkSvece" runat="server" AutoPostBack="True" OnCheckedChanged="checkSvece_CheckedChanged" /></div>
                    </div>

                    <div class="item-compl">
                        <div class="cont-item-3">
                            <p class="item">PREVOZ</p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkPrevoz" runat="server" AutoPostBack="True" OnCheckedChanged="checkPrevoz_CheckedChanged" /></div>
                    </div>

                    <div class="item-compl">
                        <div class="cont-item-4">
                            <p class="item">SANDUCI</p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkSanduci" runat="server" AutoPostBack="True" OnCheckedChanged="checkSanduci_CheckedChanged" /></div>
                    </div>

                    <div class="item-compl">
                        <div class="cont-item-5">
                            <p class="item">NARIKAČE</p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkNarikace" runat="server" AutoPostBack="True" OnCheckedChanged="checkNarikace_CheckedChanged" /></div>
                    </div>

                    <div class="item-compl">
                        <div class="cont-item-6">
                            <p class="item">KOPAČI
                            </p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkKopaci" runat="server" AutoPostBack="True" OnCheckedChanged="checkKopaci_CheckedChanged" /></div>
                    </div>

                    <div class="item-compl">
                        <div class="cont-item-7">
                            <p class="item">FULL PAKET</p>
                        </div>
                        <div class="choose"><p style="font-size: 20px; margin: 0px;">Želim ovu uslugu</p><asp:CheckBox ID="checkFull" runat="server" AutoPostBack="True" OnCheckedChanged="checkFull_CheckedChanged" /></div>
                    </div>
                </div>
            </div>
            <div style="width: 100%; display: flex; justify-content: flex-end; color: #FFD48A; font-family: 'Playfair Display', serif;">
                            <asp:TextBox CssClass="txtBox" runat="server" placeholder="UKUPNA CENA" ID="txtCena" ReadOnly="True"></asp:TextBox>
                        </div>
            <div class="cont-two">
                <div class="head-two">
                    <p class="logo">UNESITE SVOJE LIČNE INFORMACIJE</p>
                </div>
                <div class="forma">
                    <asp:TextBox ID="txtIme" runat="server" placeholder="ime i prezime" style="border: 2px solid #FFD48A; border-top: 0px; border-left: 0px; border-right: 0px; background: transparent; color: #FFD48A; font-size: 20px; font-family: 'Playfair Display', serif;"></asp:TextBox>
                    <asp:TextBox ID="txtTelefon" runat="server" placeholder="broj telefona" style="border: 2px solid #FFD48A; border-top: 0px; border-left: 0px; border-right: 0px; background: transparent; color: #FFD48A; font-size: 20px; font-family: 'Playfair Display', serif;"></asp:TextBox>
                    <asp:TextBox ID="txtAdresa" runat="server" placeholder="adresa" style="border: 2px solid #FFD48A; border-top: 0px; border-left: 0px; border-right: 0px; background: transparent; color: #FFD48A; font-size: 20px; font-family: 'Playfair Display', serif;"></asp:TextBox>
                </div>
                <div class="dugme">
                    <asp:Button ID="btnPoruci" runat="server" Text="PORUČI" CssClass="dugme-poruci" OnClick="btnPoruci_Click"/>
                </div>
            </div>
        </div>
    </div>
        <div>
        </div>
    </form>
</body>
</html>
