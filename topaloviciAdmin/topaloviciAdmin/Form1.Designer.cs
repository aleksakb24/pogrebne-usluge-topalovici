﻿namespace topaloviciAdmin
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonDodajGroblje = new System.Windows.Forms.Button();
            this.listBoxGroblje = new System.Windows.Forms.ListBox();
            this.buttonUpdateGroblje = new System.Windows.Forms.Button();
            this.buttonBrisiGroblje = new System.Windows.Forms.Button();
            this.listBoxParcele = new System.Windows.Forms.ListBox();
            this.buttonAzurirajParcelu = new System.Windows.Forms.Button();
            this.buttonDodajParcelu = new System.Windows.Forms.Button();
            this.buttonAzurirajPreminulog = new System.Windows.Forms.Button();
            this.buttonDodajPreminulog = new System.Windows.Forms.Button();
            this.listBoxPreminuli = new System.Windows.Forms.ListBox();
            this.buttonBrisiParcelu = new System.Windows.Forms.Button();
            this.buttonBrisiPreminulog = new System.Windows.Forms.Button();
            this.buttonSpisakUsluga = new System.Windows.Forms.Button();
            this.buttonSpisakPorudzbina = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonDodajGroblje
            // 
            this.buttonDodajGroblje.Location = new System.Drawing.Point(171, 26);
            this.buttonDodajGroblje.Name = "buttonDodajGroblje";
            this.buttonDodajGroblje.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajGroblje.TabIndex = 0;
            this.buttonDodajGroblje.Text = "Dodaj";
            this.buttonDodajGroblje.UseVisualStyleBackColor = true;
            this.buttonDodajGroblje.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBoxGroblje
            // 
            this.listBoxGroblje.FormattingEnabled = true;
            this.listBoxGroblje.Location = new System.Drawing.Point(12, 26);
            this.listBoxGroblje.Name = "listBoxGroblje";
            this.listBoxGroblje.Size = new System.Drawing.Size(139, 199);
            this.listBoxGroblje.TabIndex = 1;
            this.listBoxGroblje.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBoxGroblje_MouseClick);
            // 
            // buttonUpdateGroblje
            // 
            this.buttonUpdateGroblje.Location = new System.Drawing.Point(171, 66);
            this.buttonUpdateGroblje.Name = "buttonUpdateGroblje";
            this.buttonUpdateGroblje.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateGroblje.TabIndex = 2;
            this.buttonUpdateGroblje.Text = "Azuriraj";
            this.buttonUpdateGroblje.UseVisualStyleBackColor = true;
            this.buttonUpdateGroblje.Click += new System.EventHandler(this.buttonUpdateGroblje_Click);
            // 
            // buttonBrisiGroblje
            // 
            this.buttonBrisiGroblje.Location = new System.Drawing.Point(171, 109);
            this.buttonBrisiGroblje.Name = "buttonBrisiGroblje";
            this.buttonBrisiGroblje.Size = new System.Drawing.Size(75, 23);
            this.buttonBrisiGroblje.TabIndex = 3;
            this.buttonBrisiGroblje.Text = "Brisi";
            this.buttonBrisiGroblje.UseVisualStyleBackColor = true;
            this.buttonBrisiGroblje.Click += new System.EventHandler(this.buttonBrisiGroblje_Click);
            // 
            // listBoxParcele
            // 
            this.listBoxParcele.FormattingEnabled = true;
            this.listBoxParcele.Location = new System.Drawing.Point(312, 26);
            this.listBoxParcele.Name = "listBoxParcele";
            this.listBoxParcele.Size = new System.Drawing.Size(120, 199);
            this.listBoxParcele.TabIndex = 4;
            this.listBoxParcele.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBoxParcele_MouseClick);
            // 
            // buttonAzurirajParcelu
            // 
            this.buttonAzurirajParcelu.Location = new System.Drawing.Point(455, 66);
            this.buttonAzurirajParcelu.Name = "buttonAzurirajParcelu";
            this.buttonAzurirajParcelu.Size = new System.Drawing.Size(75, 23);
            this.buttonAzurirajParcelu.TabIndex = 6;
            this.buttonAzurirajParcelu.Text = "Azuriraj";
            this.buttonAzurirajParcelu.UseVisualStyleBackColor = true;
            this.buttonAzurirajParcelu.Click += new System.EventHandler(this.buttonAzurirajParcelu_Click);
            // 
            // buttonDodajParcelu
            // 
            this.buttonDodajParcelu.Location = new System.Drawing.Point(455, 26);
            this.buttonDodajParcelu.Name = "buttonDodajParcelu";
            this.buttonDodajParcelu.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajParcelu.TabIndex = 5;
            this.buttonDodajParcelu.Text = "Dodaj";
            this.buttonDodajParcelu.UseVisualStyleBackColor = true;
            this.buttonDodajParcelu.Click += new System.EventHandler(this.buttonDodajParcelu_Click);
            // 
            // buttonAzurirajPreminulog
            // 
            this.buttonAzurirajPreminulog.Location = new System.Drawing.Point(742, 66);
            this.buttonAzurirajPreminulog.Name = "buttonAzurirajPreminulog";
            this.buttonAzurirajPreminulog.Size = new System.Drawing.Size(75, 23);
            this.buttonAzurirajPreminulog.TabIndex = 9;
            this.buttonAzurirajPreminulog.Text = "Azuriraj";
            this.buttonAzurirajPreminulog.UseVisualStyleBackColor = true;
            this.buttonAzurirajPreminulog.Click += new System.EventHandler(this.buttonAzurirajPreminulog_Click);
            // 
            // buttonDodajPreminulog
            // 
            this.buttonDodajPreminulog.Location = new System.Drawing.Point(742, 26);
            this.buttonDodajPreminulog.Name = "buttonDodajPreminulog";
            this.buttonDodajPreminulog.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajPreminulog.TabIndex = 8;
            this.buttonDodajPreminulog.Text = "Dodaj";
            this.buttonDodajPreminulog.UseVisualStyleBackColor = true;
            this.buttonDodajPreminulog.Click += new System.EventHandler(this.buttonDodajPreminulog_Click);
            // 
            // listBoxPreminuli
            // 
            this.listBoxPreminuli.FormattingEnabled = true;
            this.listBoxPreminuli.Location = new System.Drawing.Point(597, 26);
            this.listBoxPreminuli.Name = "listBoxPreminuli";
            this.listBoxPreminuli.Size = new System.Drawing.Size(120, 199);
            this.listBoxPreminuli.TabIndex = 7;
            // 
            // buttonBrisiParcelu
            // 
            this.buttonBrisiParcelu.Location = new System.Drawing.Point(455, 109);
            this.buttonBrisiParcelu.Name = "buttonBrisiParcelu";
            this.buttonBrisiParcelu.Size = new System.Drawing.Size(75, 23);
            this.buttonBrisiParcelu.TabIndex = 10;
            this.buttonBrisiParcelu.Text = "Brisi";
            this.buttonBrisiParcelu.UseVisualStyleBackColor = true;
            this.buttonBrisiParcelu.Click += new System.EventHandler(this.buttonBrisiParcelu_Click);
            // 
            // buttonBrisiPreminulog
            // 
            this.buttonBrisiPreminulog.Location = new System.Drawing.Point(742, 109);
            this.buttonBrisiPreminulog.Name = "buttonBrisiPreminulog";
            this.buttonBrisiPreminulog.Size = new System.Drawing.Size(75, 23);
            this.buttonBrisiPreminulog.TabIndex = 11;
            this.buttonBrisiPreminulog.Text = "Brisi";
            this.buttonBrisiPreminulog.UseVisualStyleBackColor = true;
            this.buttonBrisiPreminulog.Click += new System.EventHandler(this.buttonBrisiPreminulog_Click);
            // 
            // buttonSpisakUsluga
            // 
            this.buttonSpisakUsluga.Location = new System.Drawing.Point(29, 287);
            this.buttonSpisakUsluga.Name = "buttonSpisakUsluga";
            this.buttonSpisakUsluga.Size = new System.Drawing.Size(122, 48);
            this.buttonSpisakUsluga.TabIndex = 12;
            this.buttonSpisakUsluga.Text = "Spisak usluga";
            this.buttonSpisakUsluga.UseVisualStyleBackColor = true;
            this.buttonSpisakUsluga.Click += new System.EventHandler(this.buttonSpisakUsluga_Click);
            // 
            // buttonSpisakPorudzbina
            // 
            this.buttonSpisakPorudzbina.Location = new System.Drawing.Point(29, 352);
            this.buttonSpisakPorudzbina.Name = "buttonSpisakPorudzbina";
            this.buttonSpisakPorudzbina.Size = new System.Drawing.Size(122, 48);
            this.buttonSpisakPorudzbina.TabIndex = 13;
            this.buttonSpisakPorudzbina.Text = "Spisak porudzbina";
            this.buttonSpisakPorudzbina.UseVisualStyleBackColor = true;
            this.buttonSpisakPorudzbina.Click += new System.EventHandler(this.buttonSpisakPorudzbina_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 450);
            this.Controls.Add(this.buttonSpisakPorudzbina);
            this.Controls.Add(this.buttonSpisakUsluga);
            this.Controls.Add(this.buttonBrisiPreminulog);
            this.Controls.Add(this.buttonBrisiParcelu);
            this.Controls.Add(this.buttonAzurirajPreminulog);
            this.Controls.Add(this.buttonDodajPreminulog);
            this.Controls.Add(this.listBoxPreminuli);
            this.Controls.Add(this.buttonAzurirajParcelu);
            this.Controls.Add(this.buttonDodajParcelu);
            this.Controls.Add(this.listBoxParcele);
            this.Controls.Add(this.buttonBrisiGroblje);
            this.Controls.Add(this.buttonUpdateGroblje);
            this.Controls.Add(this.listBoxGroblje);
            this.Controls.Add(this.buttonDodajGroblje);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonDodajGroblje;
        private System.Windows.Forms.ListBox listBoxGroblje;
        private System.Windows.Forms.Button buttonUpdateGroblje;
        private System.Windows.Forms.Button buttonBrisiGroblje;
        private System.Windows.Forms.ListBox listBoxParcele;
        private System.Windows.Forms.Button buttonAzurirajParcelu;
        private System.Windows.Forms.Button buttonDodajParcelu;
        private System.Windows.Forms.Button buttonAzurirajPreminulog;
        private System.Windows.Forms.Button buttonDodajPreminulog;
        private System.Windows.Forms.ListBox listBoxPreminuli;
        private System.Windows.Forms.Button buttonBrisiParcelu;
        private System.Windows.Forms.Button buttonBrisiPreminulog;
        private System.Windows.Forms.Button buttonSpisakUsluga;
        private System.Windows.Forms.Button buttonSpisakPorudzbina;
    }
}

