﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace topaloviciAdmin
{
    public partial class CenaUslugeForm : Form
    {
        SpisakUslugaForm parent;
        Usluga usluga;
        int cena;
        public CenaUslugeForm(SpisakUslugaForm p,Usluga u)
        {
            InitializeComponent();
            usluga = u;
            cena = usluga.cena;
            textBox1.Text = cena.ToString();
            parent = p;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Unesite cenu");
            }
            else { 
                int c= Int32.Parse(textBox1.Text);
                usluga.cena = c;
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");

                var collection = db.GetCollection<Usluga>("Usluga");
                collection.Save(usluga);
                parent.osvezi();
                this.Close();
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
    }
}
