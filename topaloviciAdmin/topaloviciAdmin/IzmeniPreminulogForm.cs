﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace topaloviciAdmin
{
    public partial class IzmeniPreminulogForm : Form
    {
        List<Groblje> groblja;
        List<Parcela> parcele;
        Form1 parent;
        Preminuli preminulaOsoba;
        public IzmeniPreminulogForm(Form1 f, Preminuli p)
        {
            InitializeComponent();
            parent = f;
            preminulaOsoba = p;
            parcele = new List<Parcela>();
            groblja = new List<Groblje>();
        }

        private void IzmeniPreminulogForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = preminulaOsoba.ime;
            textBox3.Text = preminulaOsoba.pol;
            textBox4.Text = preminulaOsoba.datRodj;
            textBox5.Text = preminulaOsoba.datSmrti;
            popuniListuGroblja();
            popuniListuParcela();
            oznaciSelektovane();
        }
        private void oznaciSelektovane()
        {
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");
            
            var collG = db.GetCollection<Groblje>("Groblje");
            var collP = db.GetCollection<Parcela>("Parcela");
            var result = collG.FindOne(Query.EQ("_id", BsonValue.Create(preminulaOsoba.groblje.Id)));
            if (result!=null)
            {
                Groblje g = result;
                comboBox1.SelectedItem = g.naziv;
                var result2 = collP.FindOne(Query.EQ("_id", BsonValue.Create(preminulaOsoba.parcela.Id)));
                if (result2 != null)
                {
                    Parcela p = result2;
                    comboBox2.SelectedItem = p.oznaka;
                }
            }
            
        }
        
        private void popuniListuGroblja()
        {
            comboBox1.Items.Clear();
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collection = db.GetCollection<Groblje>("Groblje");
            groblja.Clear();
            MongoCursor<Groblje> gr = collection.FindAll();
            groblja = gr.ToList<Groblje>();
            foreach (Groblje g in groblja)
            {
                comboBox1.Items.Add(g.naziv);
            }
        }
        private void popuniListuParcela()
        {
            comboBox2.Items.Clear();
            var connString = "mongodb://localhost/?safe=true";
            var server = MongoServer.Create(connString);
            var db = server.GetDatabase("Topalovici");

            var collectionGroblje = db.GetCollection<Groblje>("Groblje");
            var collectionParcela = db.GetCollection<Parcela>("Parcela");
            if (comboBox1.SelectedIndex == -1)
            {
                var queryParcela = from parcela in collectionParcela.AsQueryable<Parcela>()
                                   where parcela.groblje.Id == preminulaOsoba.groblje.Id
                                   select parcela;
                parcele = queryParcela.ToList<Parcela>();
                foreach (Parcela p in parcele)
                    comboBox2.Items.Add(p.oznaka);
            }
            else
            {
                Groblje g = groblja[comboBox1.SelectedIndex];
                var queryParcela = from parcela in collectionParcela.AsQueryable<Parcela>()
                                   where parcela.groblje.Id == g._id
                                   select parcela;
                parcele = queryParcela.ToList<Parcela>();
                foreach (Parcela p in parcele)
                    comboBox2.Items.Add(p.oznaka);
            }

            
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
            {
                MessageBox.Show("Morate uneti sva polja");
            }
            else
            {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");
                var coll = db.GetCollection<Preminuli>("Preminuli");
                preminulaOsoba.ime = textBox1.Text;
                preminulaOsoba.pol = textBox3.Text;
                preminulaOsoba.datRodj = textBox4.Text;
                preminulaOsoba.datSmrti = textBox5.Text;

                if (preminulaOsoba.groblje.Id != groblja[comboBox1.SelectedIndex]._id)
                {
                    preminulaOsoba.groblje = new MongoDBRef("groblje", groblja[comboBox1.SelectedIndex]._id);
                    preminulaOsoba.parcela = new MongoDBRef("parcela", parcele[comboBox2.SelectedIndex]._id);
                }
                else if (preminulaOsoba.parcela.Id != parcele[comboBox2.SelectedIndex]._id)
                {
                    preminulaOsoba.parcela = new MongoDBRef("parcela", parcele[comboBox2.SelectedIndex]._id);
                }
                coll.Save(preminulaOsoba);
                parent.popuniListBoxGroblje();
                this.Close();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.SelectedIndex = -1;
            popuniListuParcela();

        }
    }
}
