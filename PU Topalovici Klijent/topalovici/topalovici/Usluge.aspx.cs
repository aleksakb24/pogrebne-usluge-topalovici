﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using topalovici.Klase;

namespace topalovici
{
    public partial class Usluge : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected bool nothingChecked()
        {
            if (checkCvece.Checked == false && checkSanduci.Checked == false && checkSvece.Checked == false && checkPrevoz.Checked == false && checkNarikace.Checked == false
                && checkKopaci.Checked == false && checkFull.Checked == false)
                return true;
            else return false;
        }

        protected bool checkEmptyFields()
        {
            if (txtIme.Text == "")
            {
                Response.Write("<script>alert('Unesite Vase ime.')</script>");
                return false;
            }

            if (txtTelefon.Text == "")
            {
                Response.Write("<script>alert('Unesite Vas broj telefona.')</script>");
                return false;
            }

            if (txtAdresa.Text == "")
            {
                Response.Write("<script>alert('Unesite Vasu adresu.')</script>");
                return false;
            }

            if (nothingChecked())
            {
                Response.Write("<script>alert('Izaberite usluge koje zelite.')</script>");
                return false;
            }

            return true;
        }

        protected int sumUsluge()
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var db = server.GetDatabase("Topalovici");

            var collection = db.GetCollection<Usluga>("Usluga");

            int sum = 0;

            if(checkCvece.Checked)
            {
                var queryCvece = Query.EQ("naziv", "Cvece");
                Usluga cvece = collection.FindOne(queryCvece);
                sum += cvece.cena;
            }

            if (checkSanduci.Checked)
            {
                var querySanduci = Query.EQ("naziv", "Sanduci");
                Usluga sanduci = collection.FindOne(querySanduci);
                sum += sanduci.cena;
            }

            if (checkSvece.Checked)
            {
                var querySvece = Query.EQ("naziv", "Svece");
                Usluga svece = collection.FindOne(querySvece);
                sum += svece.cena;
            }

            if (checkPrevoz.Checked)
            {
                var queryPrevoz = Query.EQ("naziv", "Prevoz");
                Usluga prevoz = collection.FindOne(queryPrevoz);
                sum += prevoz.cena;
            }

            if (checkNarikace.Checked)
            {
                var queryNarikace = Query.EQ("naziv", "Narikace");
                Usluga narikace = collection.FindOne(queryNarikace);
                sum += narikace.cena;
            }

            if (checkKopaci.Checked)
            {
                var queryKopaci = Query.EQ("naziv", "Kopaci");
                Usluga kopaci = collection.FindOne(queryKopaci);
                sum += kopaci.cena;
            }

            if (checkFull.Checked)
            {
                var queryFull = Query.EQ("naziv", "Full Paket");
                Usluga full = collection.FindOne(queryFull);
                sum += full.cena;
            }

            return sum;
        }

        protected List<string> listUsluge()
        {
            List<string> usluge = new List<string>();

            if (checkCvece.Checked)
            {
                usluge.Add("Cvece");
            }

            if (checkSanduci.Checked)
            {
                usluge.Add("Sanduci");
            }

            if (checkSvece.Checked)
            {
                usluge.Add("Svece");
            }

            if (checkPrevoz.Checked)
            {
                usluge.Add("Prevoz");
            }

            if (checkNarikace.Checked)
            {
                usluge.Add("Narikace");
            }

            if (checkKopaci.Checked)
            {
                usluge.Add("Kopaci");
            }

            if (checkFull.Checked)
            {
                usluge.Add("Full");
            }

            return usluge;
        }

        protected void checkCvece_CheckedChanged(object sender, EventArgs e)
        {
            txtCena.Text = sumUsluge().ToString();
        }

        protected void checkSvece_CheckedChanged(object sender, EventArgs e)
        {
            txtCena.Text = sumUsluge().ToString();
        }

        protected void checkPrevoz_CheckedChanged(object sender, EventArgs e)
        {
            txtCena.Text = sumUsluge().ToString();
        }

        protected void checkSanduci_CheckedChanged(object sender, EventArgs e)
        {
            txtCena.Text = sumUsluge().ToString();
        }

        protected void checkNarikace_CheckedChanged(object sender, EventArgs e)
        {
            txtCena.Text = sumUsluge().ToString();
        }

        protected void checkKopaci_CheckedChanged(object sender, EventArgs e)
        {
            txtCena.Text = sumUsluge().ToString();
        }


        protected void checkFull_CheckedChanged(object sender, EventArgs e)
        {
            if(checkFull.Checked == true)
            {
                checkCvece.Enabled = false;
                checkSanduci.Enabled = false;
                checkSvece.Enabled = false;
                checkPrevoz.Enabled = false;
                checkNarikace.Enabled = false;
                checkKopaci.Enabled = false;

                checkCvece.Checked = false;
                checkSanduci.Checked = false;
                checkSvece.Checked = false;
                checkPrevoz.Checked = false;
                checkNarikace.Checked = false;
                checkKopaci.Checked = false;
            }
            else
            {
                checkCvece.Enabled = true;
                checkSanduci.Enabled = true;
                checkSvece.Enabled = true;
                checkPrevoz.Enabled = true;
                checkNarikace.Enabled = true;
                checkKopaci.Enabled = true;

            }

            txtCena.Text = sumUsluge().ToString();
        }



        protected void btnPoruci_Click(object sender, EventArgs e)
        {
            var connectionString = "mongodb://localhost/?safe=true";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var db = server.GetDatabase("Topalovici");

            var collection = db.GetCollection<Porudzbina>("Porudzbina");

            if (checkEmptyFields())
            {
                List<string> usluge = new List<string>();
                usluge = listUsluge();
                Porudzbina porudzina = new Porudzbina {ime = txtIme.Text, telefon = txtTelefon.Text, adresa = txtAdresa.Text, racun = sumUsluge(), usluge = usluge };

                collection.Insert(porudzina);

                Response.Write("<script>alert('Uspesna porudzbina!')</script>");
            }
        }

        protected void btnBack_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("WebForm1.aspx");
        }

    }
}