﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;

namespace topalovici.Klase
{
    public class Usluga
    {
        public ObjectId _id { get; set; }
        public string naziv { get; set; }
        public int cena { get; set; }
    }
}