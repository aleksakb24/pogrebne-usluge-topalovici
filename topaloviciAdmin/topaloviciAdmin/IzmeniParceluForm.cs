﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Driver.Builders;

namespace topaloviciAdmin
{
    public partial class IzmeniParceluForm : Form
    {
        Form1 parent;
        String oznaka;
        public IzmeniParceluForm(Form1 f, String o)
        {
            InitializeComponent();
            parent = f;
            oznaka = o;
        }

        private void IzmeniParceluForm_Load(object sender, EventArgs e)
        {
            textBox1.Text = oznaka;
        }

        private void buttonAddParcela_Click(object sender, EventArgs e)
        {
            if (textBox1.Text=="") 
            {
                MessageBox.Show("Morate da unesete oznaku");
            }
            else
            { 
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");
                var collectionParcela = db.GetCollection<Parcela>("Parcela");
                if (textBox1.Text == oznaka)
                {
                    MessageBox.Show("Niste promenili oznaku");
                }
                else 
                { 
                    var query = Query.EQ("oznaka", BsonValue.Create(oznaka));
                    var update = MongoDB.Driver.Builders.Update.Set("oznaka", BsonValue.Create(textBox1.Text));
                    collectionParcela.Update(query, update);
                    MessageBox.Show("Izmenjena parcela");
                    parent.selektovanaParcela = textBox1.Text;
                    parent.popuniListBoxGroblje();
                    this.Close();
                }
            }

        }
    }
}
