﻿using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using topaloviciAdmin.Collections;
using MongoDB.Driver.Builders;

namespace topaloviciAdmin
{
    public partial class DodajGrobljeForm : Form
    {
        private Form1 parent;
        public bool dodaj;
        public String naziv;
        public DodajGrobljeForm(Form1 f)
        {
            InitializeComponent();
            this.parent = f;
            label3.Text = "Dodaj groblje";
           
        }

        private void buttonAddGroblje_Click(object sender, EventArgs e)
        {
            if (textBoxAdresa.Text == "" || textBoxNaziv.Text == "")
            {
                label1.BackColor = Color.Red;
                label2.BackColor = Color.Red;
                MessageBox.Show("Popunite oba polja");
            }
            else
            {
                dodajGroblje();
                this.Close();
            }

        }

        private void dodajGroblje()
        {
                var connString = "mongodb://localhost/?safe=true";
                var server = MongoServer.Create(connString);
                var db = server.GetDatabase("Topalovici");

                var coll = db.GetCollection<Groblje>("Groblje");
                var result = coll.Find(Query.EQ("adresa", BsonValue.Create(textBoxAdresa.Text)));
                if (result.Size() == 0)
                {
                    Groblje g = new Groblje { naziv = textBoxNaziv.Text, adresa = textBoxAdresa.Text };
                    coll.Insert(g);
                }
                else
                {
                    MessageBox.Show("Groblje na toj lokaciji vec postoji");
                }
                parent.popuniListBoxGroblje();
                       
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBoxAdresa_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxNaziv_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
